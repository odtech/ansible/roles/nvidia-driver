# NVIDIA Driver

Installs the NVIDIA drivers

## Example Usage
### Playbook.yml
```yaml
- hosts: all
  gather_facts: true
  roles:
    - role: nvidia-driver
```
### Variables
```yaml
nvidia_driver_version: "nvidia-driver-410"
```
### Requirements.yml
```yaml
- src: https://gitlab.com/odtech/ansible/roles/nvidia-driver.git
  scm: git
  version: master
```
## Authors

* **Andrew Strangwood** - [ODTech](https://gitlab.com/odtech)

See also the list of [contributors](CONTRIBUTING.md) who participated in this project.

## License

This project is licensed under the BSD-3-Clause License - see the [LICENSE](LICENSE) file for details

## Links 

[Graphics Driver PPA](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa)